package com.example.ltrojanowski.sqldelighttest.data.model;

import com.google.auto.value.AutoValue;
import com.squareup.sqldelight.RowMapper;

/**
 * Created by ltrojanowski on 22.08.2017.
 */

@AutoValue
public abstract class Post implements PostModel {

    public static final Factory<Post> FACTORY = new Factory<>(AutoValue_Post::new);
    public static final RowMapper<Post> MAPPER = FACTORY.selectAllMapper();
    public static final String SELECT_ALL = FACTORY.selectAll().statement;

    public static final RowMapper<PostWithTags> POST_WITH_TAGS_MAPPER =
            FACTORY.selectWithTagsMapper(AutoValue_Post_PostWithTags::new, PostTag.FACTORY, Tag.FACTORY);
    public static final String SELECT_ALL_WITH_TAGS = FACTORY.selectWithTags().statement;

    @AutoValue
    public abstract static class PostWithTags implements SelectWithTagsModel<Post, PostTag, Tag> { }
}
