package com.example.ltrojanowski.sqldelighttest;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ltrojanowski.sqldelighttest.data.model.Post;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by ltrojanowski on 21.08.2017.
 */

public class PostsRecyclerViewAdapter extends RecyclerView.Adapter<PostsRecyclerViewAdapter.ViewHolder> {

    private Context mContext;
    private List<PostViewModel> postsDataset;

    PostsRecyclerViewAdapter(Context context) {
        mContext = context;
        postsDataset = new ArrayList<>();
    }

    public void setPostsDataset(List<PostViewModel> mPostsDataset) {
        postsDataset.clear();
        postsDataset.addAll(mPostsDataset);
        this.notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(mContext)
                    .inflate(R.layout.post_layout, parent, false);

        PostsRecyclerViewAdapter.ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PostViewModel postModel = postsDataset.get(position);
        holder.title.setText(postModel.getTitle());
        holder.posts.setText(postModel.getPost());
        holder.tags.setText("#" + TextUtils.join(" #" , postModel.getTags()));
    }

    @Override
    public int getItemCount() {
        return postsDataset.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView title;
        public TextView posts;
        public TextView tags;

        public ViewHolder(View root) {
            super(root);
            title = (TextView) root.findViewById(R.id.title_text_view);
            posts = (TextView) root.findViewById(R.id.post_text_view);
            tags = (TextView) root.findViewById(R.id.tags_text_view);
        }
    }


}
