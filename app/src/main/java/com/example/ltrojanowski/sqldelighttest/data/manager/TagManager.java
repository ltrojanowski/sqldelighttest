package com.example.ltrojanowski.sqldelighttest.data.manager;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.ltrojanowski.sqldelighttest.data.model.Tag;
import com.example.ltrojanowski.sqldelighttest.data.model.TagModel;

/**
 * Created by ltrojanowski on 23.08.2017.
 */

public class TagManager {
    public Tag.InsertRow insertRow;

    public TagManager(SQLiteOpenHelper helper) {
        SQLiteDatabase db = helper.getWritableDatabase();
        insertRow = new TagModel.InsertRow(db);
    }
}
