package com.example.ltrojanowski.sqldelighttest.data.manager;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.ltrojanowski.sqldelighttest.data.model.Post;


/**
 * Created by ltrojanowski on 23.08.2017.
 */

public class PostManager {
    public final Post.InsertRow insertRow;

    public PostManager(SQLiteOpenHelper helper) {
        SQLiteDatabase db = helper.getWritableDatabase();
        insertRow = new Post.InsertRow(db);
    }
}
