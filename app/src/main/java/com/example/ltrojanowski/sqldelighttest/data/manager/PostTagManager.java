package com.example.ltrojanowski.sqldelighttest.data.manager;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.ltrojanowski.sqldelighttest.data.model.PostTag;

/**
 * Created by ltrojanowski on 23.08.2017.
 */

public class PostTagManager {
    public PostTag.InsertRow insertRow;

    public PostTagManager(SQLiteOpenHelper helper) {
        SQLiteDatabase db = helper.getWritableDatabase();
        insertRow = new PostTag.InsertRow(db);
    }
}
