package com.example.ltrojanowski.sqldelighttest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ltrojanowski on 21.08.2017.
 */

public class PostViewModel {

    private String title;
    private String post;
    private List<String> tags;

    public PostViewModel() { }

    public PostViewModel(String title, String post) {
        this.title = title;
        this.post = post;
        this.tags = new ArrayList<>();
    }

    public boolean isEmpty() {
        return title.isEmpty() && post.isEmpty() && tags.isEmpty();
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public void addTag(String tag) {
        this.tags.add(tag);
    }

    public String getTitle() {
        return title;
    }

    public String getPost() {
        return post;
    }

    public List<String> getTags() {
        return tags;
    }

}
