# README #



### A sample app using sqlDelight and sqlBrite. ###

This repository contains a very simple example of sqlDelight with sqlBrite.
sqlBrite v.2.0.0
sqlDelight v.0.6.1

### How to set up a similar project? ###

In the project level `.gradle` file add the following lines to the depenedencies.

```
    dependencies {

        //...

        classpath 'com.squareup.sqldelight:gradle-plugin:0.6.1'

        classpath 'com.neenbedankt.gradle.plugins:android-apt:1.4'
    }
```

In the app level `.gradle` file add the following at the top of your file.
```
    buildscript {
        repositories {
            mavenCentral()
        }
    
        dependencies {
            classpath 'me.tatarka:gradle-retrolambda:3.7.0'
        }
    }
    
    // Required because retrolambda is on maven central
    repositories {
        mavenCentral()
    }
    apply plugin: 'com.android.application'
    apply plugin: 'me.tatarka.retrolambda'
    apply plugin: 'com.squareup.sqldelight'
    apply plugin: 'com.neenbedankt.android-apt'
```

as well as these dependencies

```
    dependencies {
    
       //..
    
        // auto value
        apt 'com.google.auto.value:auto-value:1.5'
        provided 'com.google.auto.value:auto-value:1.5'
    
        compile 'io.reactivex.rxjava2:rxandroid:2.0.1'
        compile 'io.reactivex.rxjava2:rxjava:2.1.2'
    
        compile 'com.squareup.sqlbrite2:sqlbrite:2.0.0'
    
    }
```

Close Intellij then click on Configure > Plugins.

![AddPlugin1](readmeImg/add_sqlDelight_plugin_1.png)

Search for sqlDelight (current version 0.6.1) add and restart.

![AddPlugin2](readmeImg/add_sqlDelight_plugin_2.png)

### Creating the database model. ###

Open your project in Project view.

![OpenInProjectView](readmeImg/open_in_project_view.png)

Inside of the projects `main` directory create an sqldelight directory.
Now recreate the project package structure inside this direcotry.
Check the immage below for refrence.

![CreateSqlDelightDirectory](readmeImg/create_sqldelight_directory.png)

Now create the `.sq` of your database model.
In case of this app we create three tables creating a many to many realtion.

```
#!sqlite3

CREATE TABLE posts (
    _id INTEGER PRIMARY KEY AUTOINCREMENT,
    title TEXT NOT NULL,
    content TEXT NOT NULL
);

-- select statements
selectAll:
SELECT * FROM posts;

selectById:
SELECT * FROM posts WHERE _id = ?;

selectWithTags:
SELECT * FROM posts
LEFT OUTER JOIN post_tag ON posts._id = post_tag.post_id
LEFT OUTER JOIN tags ON post_tag.tag_id = tags._id
GROUP BY tags._id
ORDER BY tags._id;

-- insert
insertRow:
INSERT INTO posts(title, content)
VALUES (?, ?);

-- delete
deletePost:
DELETE FROM posts WHERE _id = ?;
```

The intellij plugin for sqlDelight provides syntax highlighting

When you're happy with your SQL hit build.
Althoug it might seem nothing happend this step generated our data model interfaces.

### Using the interfaces ###

The interfaces aren't of much use on their own. We now need to instantiate these models.
This is done under our previously prepared `/java/data/model/` path.
The use of AutoValue and Retrolambda makes this step much simple.

```
#!java

@AutoValue
public abstract class Post implements PostModel {

    public static final Factory<Post> FACTORY = new Factory<>(AutoValue_Post::new);
    public static final RowMapper<Post> MAPPER = FACTORY.selectAllMapper();
    public static final String SELECT_ALL = FACTORY.selectAll().statement;

    public static final RowMapper<PostWithTags> POST_WITH_TAGS_MAPPER =
            FACTORY.selectWithTagsMapper(AutoValue_Post_PostWithTags::new, PostTag.FACTORY, Tag.FACTORY);
    public static final String SELECT_ALL_WITH_TAGS = FACTORY.selectWithTags().statement;

    @AutoValue
    public abstract static class PostWithTags implements SelectWithTagsModel<Post, PostTag, Tag> { }
}
```

### Creating the SQLiteOpenHelper ###

If you had a look at he generated files you noticed they contain all the SQL statements,
necassary to create the SQLiteOpenHelper. This reduces the chance of making a mistake,
while creating the helper, as well as the need to change anything except the version number,
after making small changes to the data structure.

```
#!java

public class SqlDelightTestDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "sqlDelightTest.db";

    final String SQL_SET_UP = "PRAGMA foreign_keys = ON";

    private static SqlDelightTestDbHelper instance;

    public static SqlDelightTestDbHelper getInstance(Context context) {
        if (null == instance) {
            instance = new SqlDelightTestDbHelper(context);
        }
        return instance;
    }

    private SqlDelightTestDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_SET_UP);
        sqLiteDatabase.execSQL(Post.CREATE_TABLE);
        sqLiteDatabase.execSQL(Tag.CREATE_TABLE);
        sqLiteDatabase.execSQL(PostTag.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + PostTag.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Post.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Tag.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
```

### Usge with sqlBrite ###

SqlDelight works great together with sqlBrite. You Query the database the same way you usally would using SqlBrite.

```
#!java

postsWithTagsDisposable = briteDatabase.createQuery(
                Arrays.asList(Post.TABLE_NAME, PostTag.TABLE_NAME, Tag.TABLE_NAME),
                Post.SELECT_ALL_WITH_TAGS)
                .mapToList(Post.POST_WITH_TAGS_MAPPER::map)
                .map(mapToViewModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(postViewModels -> postsRecyclerViewAdapter.setPostsDataset(postViewModels));
```

Notice the ease with which we can map the results of our query to the row models.