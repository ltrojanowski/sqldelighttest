package com.example.ltrojanowski.sqldelighttest.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.ltrojanowski.sqldelighttest.data.model.Post;
import com.example.ltrojanowski.sqldelighttest.data.model.PostTag;
import com.example.ltrojanowski.sqldelighttest.data.model.Tag;

/**
 * Created by ltrojanowski on 21.08.2017.
 */

public class SqlDelightTestDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "sqlDelightTest.db";

    final String SQL_SET_UP = "PRAGMA foreign_keys = ON";

    private static SqlDelightTestDbHelper instance;

    public static SqlDelightTestDbHelper getInstance(Context context) {
        if (null == instance) {
            instance = new SqlDelightTestDbHelper(context);
        }
        return instance;
    }

    private SqlDelightTestDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_SET_UP);
        sqLiteDatabase.execSQL(Post.CREATE_TABLE);
        sqLiteDatabase.execSQL(Tag.CREATE_TABLE);
        sqLiteDatabase.execSQL(PostTag.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + PostTag.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Post.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Tag.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
