package com.example.ltrojanowski.sqldelighttest.data.model;

import com.google.auto.value.AutoValue;

/**
 * Created by ltrojanowski on 22.08.2017.
 */

@AutoValue
public abstract class Tag implements TagModel {

    public static final Factory<Tag> FACTORY = new Factory<>(AutoValue_Tag::new);
}
