package com.example.ltrojanowski.sqldelighttest.data.model;

import com.google.auto.value.AutoValue;

/**
 * Created by ltrojanowski on 22.08.2017.
 */

@AutoValue
public abstract class PostTag implements PostTagModel {

    public static final Factory<PostTag> FACTORY = new Factory<>(AutoValue_PostTag::new);
}
