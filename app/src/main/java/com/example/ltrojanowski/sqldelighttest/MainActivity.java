package com.example.ltrojanowski.sqldelighttest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.ltrojanowski.sqldelighttest.data.SqlDelightTestDbHelper;
import com.example.ltrojanowski.sqldelighttest.data.manager.PostManager;
import com.example.ltrojanowski.sqldelighttest.data.manager.PostTagManager;
import com.example.ltrojanowski.sqldelighttest.data.manager.TagManager;
import com.example.ltrojanowski.sqldelighttest.data.model.Post;
import com.example.ltrojanowski.sqldelighttest.data.model.PostTag;
import com.example.ltrojanowski.sqldelighttest.data.model.Tag;
import com.squareup.sqlbrite2.BriteDatabase;
import com.squareup.sqlbrite2.SqlBrite;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private RecyclerView postsRecyclerView;
    private PostsRecyclerViewAdapter postsRecyclerViewAdapter;
    private RecyclerView.LayoutManager postsRecyclerViewLayoutManager;

    BriteDatabase briteDatabase;
    PostManager postManager;
    TagManager tagManager;
    PostTagManager postTagManager;

    Disposable postsWithTagsDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        postsRecyclerView = (RecyclerView) findViewById(R.id.posts_recycle_view);

        postsRecyclerViewLayoutManager = new LinearLayoutManager(this);

        postsRecyclerViewAdapter = new PostsRecyclerViewAdapter(this);
        postsRecyclerView.setAdapter(postsRecyclerViewAdapter);
        postsRecyclerView.setLayoutManager(postsRecyclerViewLayoutManager);

        SqlBrite sqlBrite = new SqlBrite.Builder().build();
        SqlDelightTestDbHelper helper = SqlDelightTestDbHelper.getInstance(this);
        briteDatabase = sqlBrite.wrapDatabaseHelper(helper, Schedulers.io());
        postManager = new PostManager(helper);
        tagManager = new TagManager(helper);
        postTagManager = new PostTagManager(helper);

        postsWithTagsDisposable = briteDatabase.createQuery(
                Arrays.asList(Post.TABLE_NAME, PostTag.TABLE_NAME, Tag.TABLE_NAME),
                Post.SELECT_ALL_WITH_TAGS)
                .mapToList(Post.POST_WITH_TAGS_MAPPER::map)
                .map(mapToViewModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(postViewModels -> postsRecyclerViewAdapter.setPostsDataset(postViewModels));

    }

    Function<List<Post.PostWithTags>, List<PostViewModel>> mapToViewModel = new Function<List<Post.PostWithTags>, List<PostViewModel>>() {
        @Override
        public List<PostViewModel> apply(@NonNull List<Post.PostWithTags> postWithTagses) throws Exception {
            List<PostViewModel> postViewModels = new ArrayList<PostViewModel>();
            if (!postWithTagses.isEmpty()) {
                Post.PostWithTags firstElement = postWithTagses.get(0);
                PostViewModel pvm = new PostViewModel(firstElement.posts().title(), firstElement.posts().content());
                long previousId = postWithTagses.get(0).posts()._id();
                for (Post.PostWithTags p : postWithTagses) {
                    if (previousId != p.posts()._id()) {
                        postViewModels.add(pvm);
                        pvm = new PostViewModel(p.posts().title(), p.posts().content());
                        previousId = p.posts()._id();
                    }
                    pvm.addTag(p.tags().name());
                }
                postViewModels.add(pvm);
            }
            return postViewModels;
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_insert) {
            insertRandomPost();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void insertRandomPost() {
        Log.d(TAG, "insert action");
        BriteDatabase.Transaction transaction = briteDatabase.newTransaction();
        try {
            postManager.insertRow.bind("Title1", "Content1");
            tagManager.insertRow.bind("Tag1");
            long postId = briteDatabase.executeInsert(Post.TABLE_NAME, postManager.insertRow.program);
            long tag1Id = briteDatabase.executeInsert(Tag.TABLE_NAME, tagManager.insertRow.program);
            tagManager.insertRow.bind("Tag2");
            long tag2Id = briteDatabase.executeInsert(Tag.TABLE_NAME, tagManager.insertRow.program);
            postTagManager.insertRow.bind(postId, tag1Id);
            briteDatabase.executeInsert(PostTag.TABLE_NAME, postTagManager.insertRow.program);
            postTagManager.insertRow.bind(postId, tag2Id);
            briteDatabase.executeInsert(PostTag.TABLE_NAME, postTagManager.insertRow.program);
            transaction.markSuccessful();
        } finally {
            transaction.end();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        postsWithTagsDisposable.dispose();
    }
}
